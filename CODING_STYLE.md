#### coding style
follow the [airbnb](https://github.com/airbnb/javascript) coding guidelines,
except that:

- for naming objects, functions, and instances we use `snake_case` instead of `camelCase`
- we allow leading underscores, but not trailing ones
- we don't use semicolons
- we use getters / setters

---

[![{website}](https://joshavanier.github.io/badges/svg/website.svg)]({https://1001.studio})
[![{unlicense}](https://joshavanier.github.io/badges/svg/unlicense.svg)]({LICENSE})